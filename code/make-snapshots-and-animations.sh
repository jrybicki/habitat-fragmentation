#!/bin/bash
./run-illustration-sim.sh final-experiments/illustration-1/ 1 0 15177458227 20638553997 1
./run-illustration-sim.sh final-experiments/illustration-2/ 10 0 55177458227 6638553997 1
./compress-animations.sh
cp final-experiments/illustration-1/examples.pdf ../figures/examples-1.pdf
cp final-experiments/illustration-2/examples.pdf ../figures/examples-2.pdf
