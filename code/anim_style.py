# -*- coding: utf-8 -*-
import sys
import json
import matplotlib
import matplotlib.cm

COMMUNITY_FILE = sys.argv[1] #"{}/input.json".format(community_fname)
#STYLE_FILE = "{}/style.json".format(rootdir)
data = json.load(open(COMMUNITY_FILE))

entities = data["entities"]
community = data["model"]

style = {}
cm = matplotlib.cm.Vega20
plot_resources=True
size = 5

def pair(s):
    return zip(community[s], entities[s])

for patch, model_id in pair("patches"):
    s = {}
    s["marker"] = '.'
    s["s"] = 4
    s["c"] = "gray"
    conf = {}
    conf["omit"] = True
    conf["shape"] = "circle"
    conf["radius"] = patch["generators"][0]["scale"]
    conf["integral"] = patch["generators"][0]["rate"]
    style[model_id] = {"style" : s, "conf" : conf}

for resource, model_id in pair("resources"):
    s["s"] = size if plot_resources else 0
    s["marker"] = '.'
    s["c"] = "black"
    style[model_id] = {"style" : s, "conf" : {"omit" : False}}

ALPHA = 0.8

for eid, (satiated, model_id) in enumerate(zip(community["species"], entities["satiated"])):
    s = { "edgecolor" : "none" , "alpha" : ALPHA }
    s["s"] = size*5
    s["marker"] = '.'
    s["c"] = cm(2*int(eid) % cm.N)
    style[model_id] = {"style" : s}

for eid, (hungry, model_id) in enumerate(zip(community["species"], entities["hungry"])):
    s = { "edgecolor" : "none", "alpha" : ALPHA }
    s["s"] = size*2.5
    s["marker"] = '.'
    s["c"] = cm(2*int(eid)+1 % cm.N)
    style[model_id] = {"style" : s}

print json.dumps(style)
