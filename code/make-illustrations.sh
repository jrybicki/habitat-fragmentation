#!/bin/bash
Rscript plot_sitedata.r
python trapezoid.py
python fragmenter.py
mkdir -p ../figures
mv sloss-matrix.pdf ../figures/
mv sloss-highlighted-panels.pdf ../figures/
mv sfar-summary.pdf ../figures/
mv sitedata-summary-*.pdf ../figures/
mv beta-summary-*.pdf ../figures/
mv fragment-examples.png ../figures/
