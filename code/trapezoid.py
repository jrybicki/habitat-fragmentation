import json
import sys
import numpy
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 8})

def approximate_integral(rs, bs):
    def slope(p,q):
        dx = q[0]-p[0]
        dy = q[1]-p[1]
        return float(dy)/float(dx)

    def make_sweep_list(rs, bs):
        # Assume Rs has the smallest x coordinate
        rs = sorted(rs)
        bs = sorted(bs)

        assert rs[0] <= bs[0]

        new_rs = []
        new_bs = []

        xs = sorted(set([p[0] for p in rs+bs]))
        heights = []
        b_slope = None
        r_slope = slope(rs[0], rs[1])

        for x in xs:
            if x == bs[0][0] and x == rs[0][0]:
                new_bs.append( bs.pop(0) )
                new_rs.append( rs.pop(0) )
                continue
            
            if x == bs[0][0]:
                # Add virtual R point
                y = (x-rs[0][0])*r_slope + rs[0][1]
                new_rs.append((x,y))

                b_slope = slope(bs[0], bs[1])
                new_bs.append( bs.pop(0) )
            elif rs[0][0] == x:
                # Add virtual B point
                if b_slope is not None:
                    y = (x-bs[0][0])*b_slope + bs[0][1]
                    new_bs.append((x,y))
                r_slope = slope(rs[0], rs[1])
                new_rs.append( rs.pop(0) )
            else:
                raise Exception("am confus")

        while new_rs[0][0] < new_bs[0][0]:
            new_rs.pop(0)
        assert len(new_rs) == len(new_bs)

        return new_rs, new_bs

    rs, bs = make_sweep_list(rs, bs)
    points = [ (p[0], p[1]-q[1]) for (p,q) in zip(rs,bs)]
    areas = []
    A = 0.0
    for p,q in zip(points, points[1:]):
        h = q[0]-p[0]
        a = h*(p[1]+q[1])/2.0
        A += a
        areas.append(a)

    return A, rs, bs

def plot_sloss_summary(data):
    A = []
    Ns = set()
    Cs = set()
    Xs = {}
    for d in data:
        rs = d['sl']
        bs = d['ls']
        bs[-1] = rs[-1] # hack; they should be the same but floats cause issues
        area, new_rs, new_bs = approximate_integral(rs, bs)
        d['area'] = area 
        n,c = d['info']
        Ns.add(n)
        Cs.add(c)
        A.append(area)
        Xs[(n,c)] = area

    A = list([Xs[v] for v in sorted(Xs.keys())])
    A = numpy.array(A)
    A = A.reshape(len(Ns),len(Cs))
    vv = max(map(abs, [A.max(), A.min()]))
    fig, ax = plt.subplots()
    heatmap = ax.pcolor(A, cmap=plt.cm.bwr_r, vmin=-vv, vmax=vv)

    fig.colorbar(heatmap)
    ax.set_yticklabels(sorted(Ns), minor=False)
    ax.set_xticklabels(sorted(Cs), minor=False)
    ax.set_yticks(numpy.arange(A.shape[0])+0.5, minor=False)
    ax.set_xticks(numpy.arange(A.shape[1])+0.5, minor=False)
    fig.gca().invert_yaxis()
    return fig

def plot_sloss_summary_matrix(datas):
    fig, axes = plt.subplots(3,3, sharex=True, sharey=True)
    vv = 0.75

    for ax, data in zip(axes.flatten(), datas):
        A = []
        Ns = set()
        Cs = set()

        for d in data['data']:
            rs = d['sl']
            bs = d['ls']
            bs[-1] = rs[-1] # hack; they should be the same but floats cause issues
            area, rs, bs = approximate_integral(rs, bs)
            d['area'] = area 
            Ns.add(d['info'][0])
            Cs.add(d['info'][1])
            A.append(area)
            print "N={} C={} I={}".format(d['info'][0], d['info'][1], area)

        A = numpy.array(A)
        A = A.reshape(5,8)
        #print A.max(), A.min()

        ax.set_yticklabels(sorted(Ns), minor=False)
        ax.set_xticklabels(["{0:g}%".format(float(s)*100) for s in sorted(Cs)], minor=False, rotation=90)
        ax.set_yticks(numpy.arange(A.shape[0])+0.5, minor=False)
        ax.set_xticks(numpy.arange(A.shape[1])+0.5, minor=False)
        heatmap = ax.pcolor(A, cmap=plt.cm.bwr_r, vmin=-vv, vmax=vv)
       
        if 'highlight' in data:
            for (x,y) in data['highlight']:
                rect = matplotlib.patches.Rectangle((x,y),1.0,1.0,linewidth=2,edgecolor='black',facecolor='none')
                ax.add_patch(rect)
        plt.xticks(rotation=90)

    coltitles = ["Passive", "Hostile", "Active"]
    rowtitles = ["Short ($\delta=1$)", "Medium ($\delta=3$)", "Long ($\delta=10$)"]
    for ax, t in zip(axes[0], coltitles):
        ax.set_title(t)

    pads = [-3, -3, -8]
    for ax, t, pad in zip(axes[:,0], rowtitles, pads):
        ax.set_ylabel(t, rotation=90, size='large')

    fig.subplots_adjust(hspace=0.1,wspace=0.1, right=0.8)

    cbar_ax = fig.add_axes([0.82, 0.12, 0.02, 0.75])
    cb = fig.colorbar(heatmap, cax=cbar_ax, ticks = [-vv, 0, vv])
    cb.ax.set_xticklabels(['-1 (negative effect)', '0 (no effect)', '1 (positive effect)']) 

    for ax in axes.flatten():
        ax.invert_yaxis()

    return fig

def plot_sloss_panel_to_ax(d, ax):
    orig_rs = d['sl']
    orig_bs = d['ls']
    orig_bs[-1] = orig_rs[-1] # hack; they should be the same but floats cause issues
    area, rs, bs = approximate_integral(orig_rs, orig_bs)
    color = 'red' if area < 0 else 'blue'
    xs = [p[0] for p in rs+bs[::-1]]
    ys = [p[1] for p in rs+bs[::-1]]

    ax.fill(xs,ys,color=color,alpha=abs(area))
    ax.set_ylim(0,64)
    ax.set_xlim(0,0.04)
    
    print "max x {} max y {}".format(max(xs),max(ys))
    ax.set_yticklabels([0,16,32,48,64], minor=False)
    ax.set_xticklabels(["0%","1%", "2%", "3%", "4%"])
    ax.set_yticks([0,16,32,48,64])
    ax.set_xticks([0,0.01,0.02,0.03,0.04])

    fig.legend(loc=4)

    ax.set_aspect(0.04/64)
#    ax.set_aspect(maxx/32.0)
    ax.plot(*zip(*orig_rs), label="SL", color='black', linewidth=4)
    ax.plot(*zip(*orig_bs), label="LS", color='black', linewidth=4)
    ax.plot(*zip(*orig_rs), label="SL", color='#ff7f0e', linewidth=2)
    ax.plot(*zip(*orig_bs), label="LS", color='#1f77b4', linewidth=2)

def plot_sloss_panel(data):
#    print data
    ns = int(len(data)**0.5)
    fig, axes = plt.subplots(3,1,sharex=True,sharey=True)

    for ax, d in zip(axes,data):
        plot_sloss_panel_to_ax(d,ax)
        ax.set_aspect('auto')
        ax.set_xlim(0.0,0.04)
        ax.set_ylim(0,64)

    fig.subplots_adjust(hspace=0.1,wspace=-0.0, right=0.8,left=0.25)

    fig.text(0.0, 0.5, 'Number of species', va='center', rotation='vertical',size='large')
    axes[-1].set_xlabel('Cumulative cover')
    return fig

if __name__ == '__main__':
    filenames = [
        "final-experiments/1A-passive-d1-s64-u100/data-sloss-0.json",
        "final-experiments/2A-hostile-d1-s64-u100/data-sloss-0.json",
        "final-experiments/3A-active-d1-s64-u100/data-sloss-0.json",
        "final-experiments/1B-passive-d3-s64-u100/data-sloss-0.json",
        "final-experiments/2B-hostile-d3-s64-u100/data-sloss-0.json",
        "final-experiments/3B-active-d3-s64-u100/data-sloss-0.json",
        "final-experiments/1C-passive-d10-s64-u100/data-sloss-0.json",
        "final-experiments/2C-hostile-d10-s64-u100/data-sloss-0.json",
        "final-experiments/3C-active-d10-s64-u100/data-sloss-0.json"
    ]

    datas = []
    scenario_index = 2
    xy_indices = [
                #(4,1), (4,2), (4,3), (4,4)
                (4,2), (4,3), (4,4)
            ]
    indices = [8*y+x for (x,y) in xy_indices]
    for sid, x in enumerate(filenames):
        identifier = x.split('/')[1][0:2]
        datas.append({
            'data' : json.load(open(x)),
            'identifier' : identifier
        })

        if sid == scenario_index:
            datas[-1]['highlight'] = xy_indices


    sub = [datas[scenario_index]['data'][i] for i in indices]

    h = 2
    w = len(indices)*h
    m = 1.4285714285714286

    fig = plot_sloss_summary_matrix(datas)
    fig.set_size_inches(4.5*m,4.5)
    fig.savefig("sloss-matrix.pdf")

    print len(sub)
    fig = plot_sloss_panel(sub)

    fig.set_size_inches(4.5*0.4,4.5)
    fig.savefig("sloss-highlighted-panels.pdf")
