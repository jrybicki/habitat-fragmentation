./prepare.sh final-experiments/1A-passive-d1-s64-u100 1 0 1 49740143
./prepare.sh final-experiments/1B-passive-d3-s64-u100 3 0 1 26490534
./prepare.sh final-experiments/1C-passive-d10-s64-u100 10 0 1 85949632
./prepare.sh final-experiments/2A-hostile-d1-s64-u100 1 1 1 77525702
./prepare.sh final-experiments/2B-hostile-d3-s64-u100 3 1 1 789359601
./prepare.sh final-experiments/2C-hostile-d10-s64-u100 10 1 1 38527684
./prepare.sh final-experiments/3A-active-d1-s64-u100 1 0 10 608222995
./prepare.sh final-experiments/3B-active-d3-s64-u100 3 0 10 356201244
./prepare.sh final-experiments/3C-active-d10-s64-u100 10 0 10 62543737

echo "OPTION 1: Run the following batches (takes a long time):"
for f in final-experiments/{1,2,3}*/batch.sh; do echo ./$f; done

echo "OPTION 2: Run using SLURM:"
for f in final-experiments/*/tmp/batch.sbatch; do echo sbatch $f; done
