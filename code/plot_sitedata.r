library(dplyr)
library(ggplot2)
library(grid)
library(gridExtra)

my_colors <- c("#d73027","#fc8d59", "#fee090","#7fbf7b","#91bfdb","#4575b4", "#000000")
args <- commandArgs(trailingOnly = TRUE)

site_files <- c(
"final-experiments/1A-passive-d1-s64-u100/sitedata.csv.gz",
"final-experiments/1B-passive-d3-s64-u100/sitedata.csv.gz",
"final-experiments/1C-passive-d10-s64-u100/sitedata.csv.gz",
"final-experiments/2A-hostile-d1-s64-u100/sitedata.csv.gz",
"final-experiments/2B-hostile-d3-s64-u100/sitedata.csv.gz",
"final-experiments/2C-hostile-d10-s64-u100/sitedata.csv.gz",
"final-experiments/3A-active-d1-s64-u100/sitedata.csv.gz",
"final-experiments/3B-active-d3-s64-u100/sitedata.csv.gz",
"final-experiments/3C-active-d10-s64-u100/sitedata.csv.gz"
)

sfar_files <- c(
"final-experiments/1A-passive-d1-s64-u100/data-sfar-0.csv",
"final-experiments/1B-passive-d3-s64-u100/data-sfar-0.csv",
"final-experiments/1C-passive-d10-s64-u100/data-sfar-0.csv",
"final-experiments/2A-hostile-d1-s64-u100/data-sfar-0.csv",
"final-experiments/2B-hostile-d3-s64-u100/data-sfar-0.csv",
"final-experiments/2C-hostile-d10-s64-u100/data-sfar-0.csv",
"final-experiments/3A-active-d1-s64-u100/data-sfar-0.csv",
"final-experiments/3B-active-d3-s64-u100/data-sfar-0.csv",
"final-experiments/3C-active-d10-s64-u100/data-sfar-0.csv"
)

betadiversity_files <- c(
"final-experiments/1A-passive-d1-s64-u100/beta-diversity.csv",
"final-experiments/1B-passive-d3-s64-u100/beta-diversity.csv",
"final-experiments/1C-passive-d10-s64-u100/beta-diversity.csv",
"final-experiments/2A-hostile-d1-s64-u100/beta-diversity.csv",
"final-experiments/2B-hostile-d3-s64-u100/beta-diversity.csv",
"final-experiments/2C-hostile-d10-s64-u100/beta-diversity.csv",
"final-experiments/3A-active-d1-s64-u100/beta-diversity.csv",
"final-experiments/3B-active-d3-s64-u100/beta-diversity.csv",
"final-experiments/3C-active-d10-s64-u100/beta-diversity.csv"
)

read_data <- function(files) {
    data <- data.frame()

    deltas <- mapply(function(x) { paste("delta==",x)}, c(1,3,10,
                1,3,10,
                1,3,10))

    modes <- c("Passive", "Passive", "Passive",
               "Hostile", "Hostile", "Hostile",
               "Active", "Active", "Active")

    i = 1
    for (f in files[1:length(files)]) {
        input_data <- read.csv(f)
        input_data$case <- as.factor(f)
        input_data$delta_val <- as.factor(deltas[i])
        input_data$mode_val <- as.factor(modes[i])

        print(f)
        print(summary(input_data))

        #print(summary(input_data))
        data <- rbind(data, input_data)
        i <- i + 1
    }
    data
}

get_theme <- function() {
        theme(legend.position='bottom',
              legend.background=element_rect(size=0, fill = "transparent", colour = "transparent"),
              legend.key=element_rect(fill = "transparent", colour = "transparent"),
              legend.key.height = unit(0.6,'lines'),
              legend.key.width = unit(0.8, 'lines'),
              legend.title = element_text(size=8),
              legend.text = element_text(size=7),
              legend.margin = margin(0),
              plot.margin = margin(0),
              panel.border = element_rect(fill=NA,color="black"),
              axis.text=element_text(size=7),
              axis.title=element_text(size=8),
              axis.line = element_blank(),
              strip.background = element_blank(),
              strip.text = element_text(size=8, hjust=0))
}

plot_sitedata <- function(files, outprefix) {
    print("Plotting sitedata")
    all_data <- read_data(files)

    for (rad in unique(all_data$sample.radius)) {
        data <- subset(all_data, sample.radius == rad)

        s <- data %>% 
             group_by(landscape.cover, fragments, case, delta_val, mode_val) %>%
             summarise(species=mean(species), sample.count=n()) %>%
            filter(sample.count > 5)

        s_avg <- data %>%
                 group_by(landscape.cover, case, delta_val, mode_val) %>%
                 summarise(species=mean(species), sample.count=n()) %>%
                filter(sample.count > 5)

        p <- ggplot(s, aes(x=log2(landscape.cover), y=log2(species), color=as.factor(fragments))) +
            geom_line() +
            geom_point(size=0.5) + 
            facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
            theme_classic() +
            xlab("log habitat cover") + 
            ylab("log #species") +
            scale_color_manual(values=my_colors, name="#fragments") + 
            geom_line(data=s_avg, aes(x=log2(landscape.cover), y=log2(species)), linetype="dashed", color="black") + 
            get_theme()

        outpdf <- paste(outprefix,"-",rad,".pdf",sep="")
        pdf(outpdf, width=5, height=5)
        print(p)
        dev.off()
    }
}

plot_sfar <- function(files, outpdf) {
    print("Plotting SFAR")
    data <- read_data(files)

    p <- ggplot(data, aes(x=log2(cover), y=log2(species), color=as.factor(fragments))) +
         geom_point(size=0.75) +
         geom_line() + 
         facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
         theme_classic() +
         xlab("log habitat cover") + 
         ylab("log #species") +
         scale_color_manual(values=my_colors, name="#fragments") + 
         get_theme()

    pdf(outpdf, width=5, height=5)
    print(p)
    dev.off()
}

plot_diversity <- function(files, outprefix) {
    print("Plotting beta diversity data")
    all_data <- read_data(files)
#    all_data <- na.omit(all_data) # FIXME check why files incomplete
    print(summary(all_data))

    for (rad in unique(all_data$sample.radius)) {
        outpdf <- paste(outprefix, "-", rad, ".pdf", sep="")
        s <- all_data %>%
                   subset(sample.radius == rad) %>%
                   subset(nonempty.sites >= 4) %>%
                   group_by(landscape.cover, fragments, case, delta_val, mode_val) %>%
                   summarise(jaccard=mean(jaccard),sorensen=mean(sorensen),euclidean=mean(euclidean), radius=mean(sample.radius), sample.count=n()) %>%
                   filter(sample.count > 5)

        print(s$sample.count)

        p <- ggplot(s, aes(x=log2(landscape.cover), y=jaccard, color=as.factor(fragments))) +
            geom_line() +
            geom_point(size=0.5) + 
            facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
            theme_classic() +
            xlab("log habitat cover") + 
            ylab("similarity") +
            scale_color_manual(values=my_colors, name="#fragments") + 
            get_theme()

        pdf(outpdf, width=5, height=5)
        print(p)
        dev.off()

        p <- ggplot(s, aes(x=log2(landscape.cover), y=log2(euclidean), color=as.factor(fragments))) +
            geom_line() +
            geom_point(size=0.5) + 
            facet_grid(delta_val ~ mode_val, labeller=label_parsed) + 
            theme_classic() +
            xlab("log habitat cover") + 
            ylab("log distance") +
            scale_color_manual(values=my_colors, name="#fragments") + 
            get_theme()

        pdf(paste(outprefix, "-euclid-", rad, ".pdf", sep=""), width=5, height=5)
        print(p)
        dev.off()
    }
}

plot_sitedata(site_files, "sitedata-summary")
plot_sfar(sfar_files, "sfar-summary.pdf")
plot_diversity(betadiversity_files, "beta-summary")
