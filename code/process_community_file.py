import json
import sys
from collections import defaultdict

community = json.load(open(sys.argv[1]))

mapping = defaultdict(list)
entity_count = 0

for s in community["species"]:
    mapping["satiated"].append(entity_count)
    entity_count += 1
    mapping["hungry"].append(entity_count)
    entity_count += 1

for p in community["patches"]:
    mapping["patches"].append(entity_count)
    entity_count += 1

for p in community["resources"]:
    mapping["resources"].append(entity_count)
    entity_count += 1


out = {
    "entities" : mapping,
    "model" : community
}
json.dump(out, sys.stdout)
