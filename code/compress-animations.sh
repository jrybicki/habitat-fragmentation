#!/bin/sh
mkdir -p ../animations
for f in ./final-experiments/illustration-*/*.gif; do
    case=$(dirname $f | cut -d'/' -f 3 | cut -d'-' -f 2)
    new="../animations/$case-$(basename $f)"
    gifsicle -i $f -o $new -O2 --resize-width 400 --resize-method catrom --colors 32
done

