import sys
import random
import copy

import sloss
import util

def get_time(snapshot_file):
    for l in open(snapshot_file):
        pass
    t = l.split(" ")[0]
    return int(float(t))

ANALYSE_PATH = "./simulator/analyse"

Rs = [2.0] # sample window size
basedirs = [bdir for b in sloss.read_data(sys.argv[1]).values() for bdir in b]

for basedir in basedirs:
    input_community_fname = "{}/community.json".format(basedir)
    output_snapshot_fname = "{}/out.snapshot".format(basedir)
    T = get_time(output_snapshot_fname)

    for R in Rs:
        analysis_output_fname = "{}/output-{}.json".format(basedir,R)
        print "{analyse} -s {t} -r {radius} -c {input} -i {snap} > {out} && gzip -f {out}".format(analyse=ANALYSE_PATH, t=T-1, radius=R, snap=output_snapshot_fname, input=input_community_fname, out=analysis_output_fname)

