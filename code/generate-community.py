import copy
import sys
import json
import random
import argparse

import community_input 
import combinations as cmb
import util

# types:
# - competitive (default)
# - dispersal range 
# - dispersal type



parameters = {
    "immigration" : 0.001,
    "mortality" : 1,
    "hunger" : 0.1,
    "consumption" : [{ "resource" : 0, "scale" : 1, "rate" : 1 }],
    "resource.rate" : 4,
    "patch.density" : 0.1,
    "patch.radius" : 2,
    "patch.turnover" : 0.1,
    "resource.decay" : 0.1,
    "birth.rate" : 1
}

simulator_parameters = {
    "time" : 100,
    "domain" : 50
}

def prepare_input(case, species_count=1, dispersal_scale=1.0, propagule_establishment=False, hops=1):

    birth_scale = dispersal_scale
    hungry_jumps = []
    if hops > 1:
        hungry_jumps.append(
            {
                "scale" : dispersal_scale/(hops-1)**0.5,
                "rate" : hops*case["mortality"]
            }
        )
        birth_scale = dispersal_scale/(hops-1)**0.5

    species_defaults = {
        "immigration" : case["immigration"],
        "immigration.only.to.fragments" : propagule_establishment,
        "mortality" : case["mortality"],
        "hunger" : case["hunger"],
        "consumption" : case["consumption"],
        "hungry.jump" : hungry_jumps,
        "satiated.jump" : [],
        "birth" : [ {
            "scale" : dispersal_scale,
            "rate" : case["birth.rate"],
            "only.fragment.establishment" : propagule_establishment
        }]
    }

    patch_birth = case["patch.density"] * case["patch.turnover"]
    patch_death = case["patch.turnover"]

    c = {
        "patches" : [ { 
            "birth" : patch_birth,
            "death" : patch_death,
            "generators" : [ { "resource" : 0, "scale" : case["patch.radius"], "rate" : case["resource.rate"] }],
            "successor" : -1
        }],
        "resources" : [{ "decay" : case["resource.decay"] }]
    }
    c["species"] = [ copy.deepcopy(species_defaults) for i in xrange(species_count) ] 

    community = community_input.make_input(c)
    community["initial.densities"] = {
            "resources" : [0],
            "patches" : [case["patch.density"]],
            "hungry" : [case["immigration"]] * species_count,
            "satiated" : [0.0] * species_count
    }
    return community

def main():
    input_data = {
        "species_count" : int(sys.argv[1]),
        "dispersal_scale" : float(sys.argv[2]),
        "propagule_establishment" : bool(int(sys.argv[3])),
        "hops" : int(sys.argv[4])
    }
        
    print json.dumps(prepare_input(parameters, **input_data), indent=2)

if __name__ == '__main__':
    main()
