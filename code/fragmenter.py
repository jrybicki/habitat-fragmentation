import sys
import collections

import numpy as np
import numpy.random as rnd

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle
import matplotlib.gridspec as gridspec

import util

def sample_areas(number_of_fragments, expected_cover, variance):
    mean = np.log(expected_cover / number_of_fragments) - variance/2.0
    return rnd.lognormal(mean, np.sqrt(variance), number_of_fragments)

def distance(p,q):
    return util.torus_distance(p,q,1)

def overlaps(point, radius, circles,isolation=0.0):
    for (q,r) in circles:
        d = r+radius+isolation
        if distance(point,q) <= d:
            return True
    return False

def contained(point, radius):
    x,y = point
    return x-radius >= 0 and x+radius <1 and y-radius >= 0 and y+radius < 1

def fragment(number_of_fragments, cover, variance,isolation=0):
    areas = sample_areas(number_of_fragments, 0.5, variance)  
    total_area = areas.sum()
    areas *= cover / total_area # normalise
    areas.sort()

    radii = np.sqrt(areas / np.pi)

    circles = []
    for r in radii[::-1]:
        p = rnd.uniform(0,1,2) #rnd.uniform(r,1-r,2)
        while overlaps(p, r, circles, isolation):# or not contained(p, r):
            p = rnd.random(2)

        circles.append((p,r))

    return circles

def area(fragments):
    if len(fragments) == 0: return 1.0
    A = 0.0
    for (p,r) in fragments:
        A += np.pi*r**2
    return A

def overflows(fragments):
    new = []
    U = 1
    for point,radius in fragments:
        x,y = point
        overflows = []

        # Check if patch overflows
        if x-radius < 0:
            overflows.append( (U+x, y) )
        if y-radius < 0:
            overflows.append( (x, U+y) )
        if x+radius > U:
            overflows.append( (x-U, y) )
        if y+radius > U:
            overflows.append( (x, y-U) )

        if x+radius > U and y+radius > U:
            overflows.append( (x-U, y-U) )

        if x-radius < 0 and y-radius < 0:
            overflows.append( (x+U, y+U) )

        if x+radius > U and y-radius < 0:
            overflows.append( (x-U, y+U) )

        if x-radius < 0 and y+radius > U:
            overflows.append( (x+U, y-U) )

        for p in overflows:
            new.append((p,radius))

    return new

def generate_patches(n, radius):
    return [(p, radius) for p in np.random.random((n,2))]

MATRIX_COLOR = 'white'
EMPTY_HABITAT_COLOR = '#efefef' 
PATCH_COLORS = ['blue','green']
PATCH_ALPHA = 0.5
ONE_PATCH_TYPE = True

def get_type(location, gradient):
    if ONE_PATCH_TYPE:
        return 1
    p = gradient_probability(location) if gradient else 0.5
    if gradient:
        p = int(p+0.5)
    if np.random.random() >= p:
        return 1
    else:
        return 0

def gradient_probability(loc):
    x,y = loc
    return abs((x-y)+0.5)

def masked_plot(ax, patches, fragments, gradient=False):
    def pc(ls, **p):
        cs = []
        for (loc, radius) in ls + overflows(ls):
            cs.append(mpl.patches.Circle(loc,radius=radius,**p))
        return cs
        #return mpl.collections.PatchCollection(cs, **p)

    # Randomise patch types
    d = collections.defaultdict(list)
    for p in patches:
        d[get_type(p[0],gradient)].append(p)

    if len(fragments) == 0:
        for v in d.keys():
            r = ax.add_patch(mpl.patches.Rectangle((0,0),1,1,facecolor=EMPTY_HABITAT_COLOR,zorder=20,alpha=0.5))
            ps = mpl.collections.PatchCollection(pc(d[v]), facecolor=PATCH_COLORS[v],alpha=PATCH_ALPHA,zorder=100)
            ax.add_collection(ps)
        return

    for f in pc(fragments, facecolor='none', edgecolor='black',zorder=10,alpha=0.5):
        r = ax.add_patch(mpl.patches.Rectangle((0,0),1,1,facecolor=EMPTY_HABITAT_COLOR,zorder=20))
        for v in d.keys():
            ps = mpl.collections.PatchCollection(pc(d[v]), facecolor=PATCH_COLORS[v],alpha=PATCH_ALPHA,zorder=100)
            c = ax.add_collection(ps)
            ax.add_patch(f)
            c.set_clip_path(f)
            r.set_clip_path(f)

def plot_grid(p, cases, shape, size,fname=True):
    np.random.seed(6666)    

    fig = plt.figure(figsize=(shape[0]*size,shape[1]*size))
    gs = gridspec.GridSpec(*shape)
    gs.update(wspace=0.05, hspace=0.2)
    plt.xlim((0,1))
    plt.ylim((0,1))
    for case,g in zip(cases,gs):
        print case
        ax = plt.subplot(g)
        ax.set_zorder(1000)
        plot_single(case, ax)
    if fname:
        plt.savefig("fragment-examples.png")
    else:
        plt.show()

def plot_single(case,ax):
    # Generate landscape
    patch_params, fragmentation_params, gradient = case
    patches = generate_patches(*patch_params)
    if fragmentation_params == None:
        fragments = []
    else:
        fragments = fragment(*fragmentation_params)
    # Plot
    #ax.set_title("$n={}, c={:.2f}$".format(fragmentation_params[0],area(fragments)))A
    ax.set_title("$C={:.2f}$".format(area(fragments)))
    #t = ax.text(0.01,0.01,"$A={:.2f}$".format(area(fragments)),zorder=500)
    #t.set_bbox(dict(facecolor='white', alpha=0.8, edgecolor='none'))
    ax.set_aspect('equal')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.add_patch(mpl.patches.Rectangle((0,0),1,1,facecolor=MATRIX_COLOR))
    for s in ax.spines.values():
        s.set_zorder(1000)
    masked_plot(ax, patches, fragments, gradient)

if __name__ == '__main__':
    p = (250,0.04) # was 250
    v = 1

    default_cases = [
        [p, None, True],
        [p, (4, 0.5, v), True],
        [p, (256, 0.5, v), False],
        [p, (1024, 0.5, v), False],
        [p, None, False],
        [p, (4, 0.3, v), False],
        [p, (64, 0.3, v), True],
        [p, (256, 0.3, v), False],
        [p, None, True],
        [p, (5, 0.1, v), False],
        [p, (10, 0.1, v), False],
        [p, (25, 0.1, v), False],
        [p, (256, 0.1, v), True],
        [p, None, False],
        [p, (5, 0.05, v), False],
        [p, (10, 0.05, v), False],
        [p, (25, 0.05, v), False],
        [p, (256, 0.05, v), True]
    ]
    cases = [
        [p, None, True],
        [p, (1, 0.32, v), True],
        [p, (1, 0.16, v), True],
        [p, (1, 0.08, v), True],
        [p, (1, 0.04, v), True],

        [p, (4, 0.32, v), True],
        [p, (16, 0.32, v), False],
        [p, (64, 0.32, v), False],
        [p, (256, 0.32, v), False],
        [p, (1024, 0.32, v), False],

        [p, (4, 0.16, v), True],
        [p, (16, 0.16, v), False],
        [p, (64, 0.16, v), False],
        [p, (256, 0.16, v), False],
        [p, (1024, 0.16, v), False],
       
        [p, (4, 0.08, v), True],
        [p, (16, 0.08, v), False],
        [p, (64, 0.08, v), False],
        [p, (256, 0.08, v), False],
        [p, (1024, 0.08, v), False],
        
        [p, (4, 0.04, v), True],
        [p, (16, 0.04, v), False],
        [p, (64, 0.04, v), False],
        [p, (256, 0.04, v), False],
        [p, (1024, 0.04, v), False]
    ]
    plot_grid(p, cases, (5,5), 2, fname=True)
