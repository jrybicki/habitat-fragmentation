#/bin/bash
mkdir -p collect-batches

echo "Prepare SLOSS"
./make-sloss.sh > collect-batches/sloss.sh
./make-slurm collect-batches/sloss.sh > collect-batches/sloss.sbatch

echo "Prepare analysis with other sampling site ranges"

./make-reanalysis.sh > collect-batches/reanalysis.sh
./batch-tools/make-slurm-script collect-batches/reanalysis.sh 1000 collect-batches/tmp-2 
./batch-tools/chunkify collect-batches/reanalysis.sh collect-batches/tmp-2/chunk 1000

echo "Prepare site data gathering"

./make-sitedata.sh > collect-batches/sitedata.sh
./make-slurm collect-batches/sitedata.sh > collect-batches/sitedata.sbatch

echo "Run the following:"
echo "sbatch collect-batches/sloss.sbatch"
echo "sbatch collect-batches/tmp-2/batch.sbatch"
echo "sbatch collect-batches/sitedata.sbatch"

