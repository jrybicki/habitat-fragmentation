#!/bin/bash

INPUT_DIR=$1
DISPERSAL=$2 # dispersal scale
ESTABLISHMENT=$3 # no establishment restriction
HOPS=$4 # single hop during the life time of a propagule (birth)
INIT_SEED=$5
SIM_SEED=$((INIT_SEED+1))

BASE_COMMUNITY="$INPUT_DIR/community.json"
BATCH="$INPUT_DIR/batch.sh"
OUT_DIR="$INPUT_DIR/out/"
TMP_DIR="$INPUT_DIR/tmp/"

U=100.0
INIT_T=300.0
SIM_T=300.0
REPLICATES=100
SPECIES=64
DT=$SIM_T

mkdir -p $INPUT_DIR
mkdir -p $OUT_DIR
python generate-community.py $SPECIES $DISPERSAL $ESTABLISHMENT $HOPS > $BASE_COMMUNITY
./init.sh $INPUT_DIR $INIT_SEED $INIT_T $U
python fragment-replicates.py $BASE_COMMUNITY $REPLICATES $OUT_DIR $SIM_SEED $INPUT_DIR/init/initial.state.txt $U $SIM_T $DT > $BATCH
./batch-tools/make-slurm-script $BATCH 1000 $TMP_DIR
./batch-tools/chunkify $BATCH $TMP_DIR/chunk 1000
echo "Batch: $BATCH"
echo "Slurm batch: $TMP_DIR/batch.sbatch"
