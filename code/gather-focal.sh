#!/bin/bash
for f in final-experiments/{1,2,3}*/out/*/*/output*.json.gz; do
    s=$(echo $f | sed "s/output/local/g")
    echo "gunzip -c $f | simulator/focal > $s"
done

