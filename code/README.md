# Supplementary material for "Habitat fragmentation and species diversity in competitive species communities"

This package contains the source code for simulator used in the manuscript:

    Habitat fragmentation and species diversity in competitive species communities
    Joel Rybicki, Nerea Abrego, and Otso Ovaskainen
    Date: June 2018

Below are the instruction on how to run the simulations and replicate the analyses.

## Requirements and dependencies

For running the simulations, you need to have a C++ compiler and the following additional software and packages installed:

* [R 3.3](https://www.r-project.org/)
    * ggplot2
    * dplyr
    * gridExtra
* [Python 2.7](https://www.python.org/)
    * numpy (1.12.0)
    * matplotlib (2.0.0)
* [ImageMagick](https://www.imagemagick.org/)
* [Boost C++ libraries](http://www.boost.org/)

The simulator also relies on the following libraries, which are included in `simulator/external/include`.

* [JSON for Modern C++](https://github.com/nlohmann/json)
* [CXXopts](https://github.com/jarro2783/cxxopts)
* [PCG random number generator for C++](https://github.com/imneme/pcg-cpp/)
* [Catch](https://github.com/philsquared/Catch.git)

The software has been tested on MacOS X 10.12 (Sierra) and on CentOS 6 (GNU/Linux). 

Note: The simulator shares code used in another project: [A model of bacterial toxin-dependent pathogenesis explains infective dose](https://bitbucket.org/jrybicki/spatial-infective-dose/)

## Generating illustrations and animations

To generate other plots, run:

    ./make-illustrations.sh

To generate the snapshot figures and animations, run:

    cd simulator/
    make
    cd ..
    ./make-snapshots-and-animations.sh

## Re-running the simulations

In case you wish to repeat the simulations yourself, follow the instructions given below. Be aware that running the simulations takes a very long time.

### Preparing and running the simulations

First, you need to generate the necessary batch files by running

    ./prepare-simulations.sh

Now you need to run the contents in the following batch files:

    ./final-experiments/1A-passive-d1-s64-u100/batch.sh
    ./final-experiments/1B-passive-d3-s64-u100/batch.sh
    ./final-experiments/1C-passive-d10-s64-u100/batch.sh
    ./final-experiments/2A-hostile-d1-s64-u100/batch.sh
    ./final-experiments/2B-hostile-d3-s64-u100/batch.sh
    ./final-experiments/2C-hostile-d10-s64-u100/batch.sh
    ./final-experiments/3A-active-d1-s64-u100/batch.sh
    ./final-experiments/3B-active-d3-s64-u100/batch.sh
    ./final-experiments/3C-active-d10-s64-u100/batch.sh

This will take a *long* time. If you have a computing cluster with SLURM installed, you can instead run the following SLURM batch files:

    sbatch final-experiments/1A-passive-d1-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/1B-passive-d3-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/1C-passive-d10-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/2B-hostile-d3-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/2C-hostile-d10-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/3A-active-d1-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/3B-active-d3-s64-u100/tmp/batch.sbatch
    sbatch final-experiments/3C-active-d10-s64-u100/tmp/batch.sbatch

### Collecting data
    
After running the above scripts, the simulations have been conducted. Now to extract some useful data from the simulations, first run:

    sh prepare-collect.sh

The script will output the commands you need to run.
