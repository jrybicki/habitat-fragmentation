import sys
import json
import util
import sloss
import csv
import re
import collections
import glob
import os

order = 0
datadir = sys.argv[1]

def get_data(datadir):
    scenario, case_id = datadir.split("/")[-2:]
    fragment_count, cover = scenario.split("-")

    community_data = json.load(open("{}/community.json".format(datadir)))
    output_fnames = glob.glob("{}/output*.json.gz".format(datadir))

    for of in output_fnames:
        count_data = json.load(util.open_file(of))
        local_fname = of.replace("output","local")
        focal_data = json.load(util.open_file(local_fname))

        if focal_data == None:
            return

        last_data = count_data["data"][-1]

        entity_mapping = sloss.get_entity_map(community_data)

        site_count = len(count_data["sample.sites"])
        assert len(count_data["sample.sites"]) == len(focal_data["data"])
        assert len(last_data["sample.counts"]) == len(focal_data["data"])

        satiated = [sloss.get_type_count("satiated", counts, entity_mapping) for counts in last_data["sample.counts"]]

        assert len(satiated) == len(focal_data["data"])

        for site_id, sdata in enumerate(satiated):
            scount = sloss.hill(sdata, order)
            data = focal_data["data"][site_id]

            assert data["x"] == count_data["sample.sites"][site_id]["x"]
            assert data["y"] == count_data["sample.sites"][site_id]["y"]

            for r in data["results"]:
                r["x"] = data["x"]
                r["y"] = data["y"]
                r["species"] = scount
                r["site.id"] = site_id
                r["landscape.fragments"] = fragment_count
                r["landscape.cover"] = cover
                r["landscape.sites"] = site_count
                r["case.id"] = case_id
                r["sample.radius"] = count_data["sample.window.radius"]
#                r["file"] = local_fname
                yield r

def get_all(dirs):
    for d in dirs:
        for r in get_data(d):
            yield r

rootdir = sys.argv[1]
pattern = re.compile(".*/([0-9]+)-(0.[0-9]+)/")
d = collections.defaultdict(list)
dirs = []

for rdir in glob.iglob(rootdir+'*/'):
    m = pattern.match(rdir)
    if m is None:
        continue
    
    for subdir in glob.iglob(rdir+'/*'):
        if len(glob.glob(subdir+"/output*.json.gz")):
            dirs.append(subdir)

data_itr = get_all(dirs)
try:
    first = data_itr.next()
except StopIteration:
    sys.exit(0)

writer = csv.DictWriter(sys.stdout, first.keys())
writer.writeheader()
writer.writerow(first)

for r in data_itr:
    writer.writerow(r)
