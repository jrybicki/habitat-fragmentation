import sys
import json
import numpy
import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as pp
import os.path
import glob
import re
import collections
import util
import trapezoid
import csv

SPECIES = 64

mpl.rcParams.update({'font.size': 6})
mpl.rc('xtick', labelsize=6) 
mpl.rc('ytick', labelsize=6) 

def get_entity_map(community):
    out = {}
    ents = community["entities"]
    for x in ["hungry", "satiated", "resources", "patches"]:
        for i, m in enumerate(ents[x]):
            out[m] = (x,i)
    return out

def get_proportions(counts):
    total = float(sum(counts))
    return numpy.array(counts) / total

def hill(ps, order=0):
    """ Compute the Hill number of given order."""
    norm = float(sum(ps))
    ps = numpy.array([p/norm for p in ps if p > 0]) # filter out zeros
    
    if order == 1: # Order 1 Hill numbers are a special case
        return sum([-p*numpy.log(p) for p in ps])
    else:
        x = 1/float(1-order)
        return (ps**order).sum() ** x

def get_fragment_areas(data):
    areas = []
    for f in data["scaled.fragments"]:
        areas.append(f['r']**2.0*numpy.pi)
    return areas

def get_type_count(typestr, counts, entity_mapping):
    new_counts = []
    for i, count in enumerate(counts):
        if entity_mapping[i][0] == typestr:
            new_counts.append(count)
    return numpy.array(new_counts)

def get_cumulative_counts(U, area_counts, order=0):
    out = []
    total_area = area_counts[0][0]
    species_counts = area_counts[0][1]
    out.append((total_area, species_counts.copy()))

    for area, counts in area_counts[1:]:
        total_area += area
        species_counts += counts
        out.append((total_area, species_counts.copy()))

    for a, s in out:
        print a, s
    U = float(U)
    N = U*U
    return numpy.array([numpy.array((a/N, hill(s, order))) for (a, s) in out])

def compute_sloss_data(all_data, entity_mapping, order=0):
    data = all_data["data"][-1]
    areas = get_fragment_areas(all_data)
    satiated = [get_type_count("satiated", counts, entity_mapping) for counts in data["fragment.counts"]]
    area_counts = zip(areas, satiated)

    # Smallest to largest
    small_to_large = sorted(area_counts)

    # Largest to smallest
    large_to_small = sorted(area_counts, reverse=True)

    return get_cumulative_counts(all_data["U"], small_to_large, order), get_cumulative_counts(all_data["U"],large_to_small, order)

def average_ls(ls):
    A = sum(ls)
    return A/len(ls)

def compute_average(basedirs, order):
    sls = []
    lss = []
    for basedir in basedirs:
        print basedir
        indata = json.load(util.open_file("{}/output.json.gz".format(basedir)))
        community = json.load(open("{}/community.json".format(basedir)))
        entity_map = get_entity_map(community)
        sloss_sl, sloss_ls = compute_sloss_data(indata, entity_map, order)
        sls.append(sloss_sl)
        lss.append(sloss_ls)
    return average_ls(sls), average_ls(lss)

def read_data(rootdir):
    pattern = re.compile(".*/([0-9]+)-(0.[0-9]+)/")
    d = collections.defaultdict(list)

    for rdir in glob.iglob(rootdir+'*/'):
        m = pattern.match(rdir)
        if m is None:
            continue

        p = m.groups() # fragments, cover
        fragments, cover = p

        for subdir in glob.iglob(rdir+'/*'):

            if len(glob.glob(subdir+"/output*.json.gz")) > 0:
                d[(int(fragments),float(cover))].append(subdir)
    return d


def plot(basedirs, ax, order):
    sloss_sl, sloss_ls = compute_average(basedirs, order)
    ax.set_ylim(0,SPECIES)
    ax.set_yticks([0, SPECIES/4.0, SPECIES/2.0, SPECIES*3.0/4.0, SPECIES])
    ax.plot(*zip(*sloss_ls), label="LS", linewidth=0.1)
    ax.plot(*zip(*sloss_sl), label="SL", linewidth=0.1)
    return sloss_ls, sloss_sl

def plot_sloss(d, order=0):
    sloss_data = []
    omitted = [p for p in d.keys() if p[0] == 1] # or p[1] < 0.01]
    for p in omitted:
        del d[p]

    fs = sorted(set((p[0] for p in d.keys())))
    fcount = len(fs)
    cs = sorted(set((p[1] for p in d.keys())))
    ccount = len(cs) 

    fig, axes = pp.subplots(fcount,ccount, sharey=True, sharex='col')

    for ax, (info, basedir) in zip(axes.flatten(),sorted(d.items())):
        lines = plot(basedir, ax, order)
        sloss_data.append({ 
            'info' : info, 
            'ls' : lines[0].tolist(), 
            'sl' : lines[1].tolist()})

    for ax, col in zip(axes[0], cs):
        ax.set_title(col)

    for i in xrange(len(axes)):
        for ax, col in zip(axes[i], cs):
            ax.set_xticks([0, col/2.0,  col])
            ax.set_xlim(0, col)

    for ax, rowlabel in zip(axes[:,0], fs):
        ax.set_ylabel(rowlabel,size='large')

    pp.legend(loc=4)
    #pp.show()
    return fig, sloss_data

def compute_sar(data, order=0):
    res = []
    for cover, basedirs in data:
        vals = []
        for basedir in basedirs:
            indata = json.load(util.open_file("{}/output.json.gz".format(basedir)))
            community = json.load(util.open_file("{}/community.json".format(basedir)))
            entity_map = get_entity_map(community)
            species_counts = get_type_count("satiated", indata["data"][-1]["total.counts"], entity_map)
            print cover, species_counts, hill(species_counts, order)
            vals.append(hill(species_counts, order))
        print cover, numpy.average(vals)
        res.append((cover, numpy.average(vals)))
    return res

def plot_sfar(d, order=0):
    sd = collections.defaultdict(list)
    for p in d.keys():
        fragments, cover = p
        sd[fragments].append( (cover, d[p]) )

    print "sd =", sd
    res = {}
    fig = pp.figure(2, figsize=(3,3))
    ax = fig.add_subplot(111)
    data = {}
    for fragments in sorted(sd.keys()):
        print "#fragments =", fragments
        res = compute_sar(sorted(sd[fragments]), order)
        data[fragments] = res
        ax.set_ylim((0,SPECIES))
        ax.set_xlim((0,0.32))
        ax.plot(*zip(*res), label=fragments, linestyle="solid")

    ax.legend()
    print "res =", res
    return fig, data

def write_sfar_csv(sfar_data, sfar_data_fname):
    w = csv.DictWriter(open(sfar_data_fname, 'w'), fieldnames = { 'fragments', 'species', 'cover' })
    w.writeheader()
    for fragments, points in sfar_data.items():
        for cover, species in points:
            w.writerow( { 
                'fragments' : fragments,
                'species' : species,
                'cover' : cover
            })


if __name__ == '__main__':
    d = read_data(sys.argv[1])
    order = 0
    if len(sys.argv) > 2:
        order = int(sys.argv[3])

    sloss_fname = "{}/sloss-{}.pdf".format(sys.argv[2],order)
    sloss_data_fname = "{}/data-sloss-{}.json".format(sys.argv[2],order)
    sloss_summary_fname = "{}/summary-sloss-{}.pdf".format(sys.argv[2],order)
    sfar_fname = "{}/sfar-{}.pdf".format(sys.argv[2],order)
    sfar_data_fname = "{}/data-sfar-{}.csv".format(sys.argv[2], order)

    # SFAR first since sloss plots erase data (hack)
    sfar_fig, sfar_data = plot_sfar(d,order)
    pp.tight_layout()
    sfar_fig.savefig(sfar_fname, size=(1,1))
    write_sfar_csv(sfar_data, sfar_data_fname)

    # SLOSS
    sloss_fig, sloss_data = plot_sloss(d,order)
    pp.tight_layout()
    sloss_fig.savefig(sloss_fname, size=(8,8))
    # Sloss data dump
    json.dump(sloss_data, open(sloss_data_fname, 'w'))
    # Sloss summary fig
    summary_fig = trapezoid.plot_sloss_summary(sloss_data)
    summary_fig.savefig(sloss_summary_fname, size=(1,1))
