#include <algorithm>    // std::sort
#include <vector> // std::vector
#include <unordered_set> 
#include <list>
#include "common.h"

#ifndef __FRAGMENT_H_
#define __FRAGMENT_H_

/*
 * Algorithms for querying whether a point hits a given list of (static) circles.
 */
namespace pp {

/* Bounding box */
class Box {
public:
    Box(Coord tl, double height) : x0(tl[0]), x1(tl[0]+height), y0(tl[1]), y1(tl[1]+height) {}
    Box(double xx0, double xx1, double yy0, double yy1): x0(xx0), x1(xx1), y0(yy0), y1(yy1) {}
    double x0, x1, y0,y1;

    double area() const {
        return (x1-x0)*(y1-y0);
    }

    bool within(double v, double min, double max) const {
        return (v >= min) && (v <= max);
    }   

    /* Check whether the intervals [a0, a1] and [b0, b1] intersect */ 
    bool line_intersects(double a0, double a1, double b0, double b1) const {
        if (a0 < b0) {
            return within(b0, a0, a1);
        } else {
            return within(a0, b0, b1);
        }
    }

    /* Check whether this box and the other box intersect */
    bool intersects(const Box &other) const {
        return (line_intersects(x0,x1,other.x0,other.x1)) &&
               (line_intersects(y0,y1,other.y0,other.y1));
    }

    bool contains(const Box &other) const {
        return (within(other.x0, x0, x1) && 
                within(other.x1, x0, x1) &&
                within(other.y0, y0, y1) &&
                within(other.y1, y0, y1));
    }

    bool contains(const Coord &point) const {
        return (within(point[0], x0, x1) &&
                within(point[1], y0, y1));
    }

    double truncate_val(double v, double m0, double m1) const {
        if (v >= m0 && v <= m1) { return v; }
        if (v < m0) { 
            return m0; 
        } else { 
            return m1; 
        }
    }

    /* Get the point closest to p on the box */
    Coord closest(const Coord &p) const {
        return Coord(truncate_val(p[0], x0, x1), truncate_val(p[1], y0, y1));
    }

    /* Compute the area of the intersection rectangle */
    double area_of_intersection(Box &other) const {
        double ix0 = fmax(x0,other.x0);
        double ix1 = fmin(x1,other.x1);
        double iy0 = fmax(y0,other.y0);
        double iy1 = fmax(y1,other.y1);

        double w = ix1-ix0;
        double h = iy1-iy0;
        if (w > 0 && h > 0) {
            return w*h;
        } else {
            return 0;
        }
    }
};

/* Circle with bounding box */
class Circle {
public:
    Circle(Coord c, double r, int id=-1) : center(c), radius(r), rsqr(r*r), area(M_PI * r*r),
                                  bounding_box( (c-r/2.0), r), identifier(id) {}
    Circle(double x, double y, double r, int id=-1) : Circle(Coord(x,y),r,id) {}
    Coord center;
    double radius, rsqr, area;
    Box bounding_box;
    int identifier; 

    /* Check whether point p belongs to the circle */
    bool contains(const Coord &p) const {
        return center.squared_distance(p) <= rsqr;
    }


    bool intersects(Box &b) const {
        if (bounding_box.intersects(b)) {
          /* bounding box intersects, check whether circle intersects */
          Coord q = b.closest(center);
          return center.squared_distance(q) <= rsqr;
        } else { 
          /* bounding box does not intersect, then neither does the circle */
          return false;
        }
    }

    bool covers(Box &b) const {
        /* Check whether the circle covers box b (on the plane) */
        if (center.squared_distance( Coord(b.x0, b.y0) ) > rsqr) return false;
        if (center.squared_distance( Coord(b.x0, b.y1) ) > rsqr) return false;
        if (center.squared_distance( Coord(b.x1, b.y0) ) > rsqr) return false;
        if (center.squared_distance( Coord(b.x1, b.y1) ) > rsqr) return false;
        return true;
    }
};

using circle_container = std::vector<Circle>;

bool cmp_circ(Circle a, Circle b) {
    if (a.radius < b.radius) return true;
    if (a.radius == b.radius && a.center[0] < b.center[0]) return true;
    if (a.radius == b.radius && a.center[0] == b.center[0] && a.center[1] < b.center[1]) return true;
    return false;
}

bool eq_circ(Circle a, Circle b) {
    return (a.radius == b.radius && a.center[0] == b.center[0] && a.center[1] == b.center[1]);
}

class CoverLookup {
public:
    CoverLookup(circle_container cs, int g, Coord Ubound) : circles(cs), granularity(g), U(Ubound), height(Ubound[0]/granularity) {
        /* Mirror circles that cross borders */
        add_mirrored();

        //std::cout << circles.size() << " circles" << std::endl;
        //circles = circle_container(circles.begin(), it);

        /* Construct bins */
        auto total_bins = granularity*granularity;
        for(auto bid=0; bid<total_bins; bid++) {
            auto cell = get_bin_from_id(bid);
            auto i = cell.first;
            auto j = cell.second;

            assert(bid == i+j*granularity);

            Box b(i*height, (i+1)*height, j*height, (j+1)*height); // bounding box of the cell
            circle_container cc; // find intersecting circles
            for(Circle c : circles) {
                //std::cout << "Circle " << c.center[0] << ", " << c.center[1] << ", " << c.radius << "\n";
                //std::cout << "Box" << b.x0 << ", " << b.x1 << ", " << b.y0 << ", " << b.y1 << std::endl;
                if (c.intersects(b)) {
                    cc.push_back(c);
                }
            }
            bins.push_back(CoverCell(b,cc));
        }
    }

    /* Check whether point p hits some stored circle */
    bool is_covered(const Coord &p) const {
        auto bid = get_bin(p);
        assert(bid >= 0 && bid < bins.size());
        return bins[bid].is_covered(p);
    }

    std::list<Circle> which_covers(const Coord &p) const {
        auto bid = get_bin(p);
        assert(bid >= 0 && bid < bins.size());
        return bins[bid].which_covers(p);
    }

    int wrap_bin(int i) const {
        if (i<0) return granularity+i;
        if (i>= granularity) return granularity-i;
        return i;
    }

    /* Checks if there is a circle within radius r from p */
    bool has_within(const Coord &p, double r) const {
        auto cdistance = int((r / height + 0.5)); // how many bins to the left/right/north/south to check


        for(auto dx = -cdistance; dx <= cdistance; dx++) {
            for(auto dy = -cdistance; dy <= cdistance; dy++) {
                int i = wrap_bin(p[0]/height + dx);
                int j = wrap_bin(p[1]/height + dx);
                auto bid = i+j*granularity; // bin to check
                
                if (bins[bid].completely_covered) { 
                    // the bin is completely covered by some circle, so don't bother finding it
                    return true;
                }
                for (auto &c : bins[bid].circles) { // check each circle in the bin
                    auto sqr_distance = (c.radius+r)*(c.radius+r);
                    if (c.center.torus_squared_distance(p, U[0]) <= sqr_distance) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    int get_bin(const Coord &c) const {
        int i = c[0]/height;
        int j = c[1]/height;
        return i+j*granularity;
    }

    std::pair<int,int> get_bin_from_id(int bid) const {
        auto i = bid % granularity;
        auto j = (bid-i)/granularity;
        return std::pair<int,int>(i,j);
    }

    circle_container get_circles() const {
        return circles;
    }
protected:

    void add_mirrored() {
        circle_container ms; 
        for(auto c : circles) { // fixme replicates
            std::vector<bool> under, over;
            std::vector<std::array<double,2>> new_coords;
            for(auto i=0; i<2; i++) {
                under.push_back((c.center[i] - c.radius) < 0);
                over.push_back((c.center[i] + c.radius) >= U[i]);
            }

            for(auto i=0; i<2; i++) {
                if (under[i]) {
                    std::array<double,2> nc = c.center.get_values();
                    nc[i] = c.center[i] + U[i];
                    new_coords.push_back(nc);
                }
                
                if (over[i]) {
                    std::array<double,2> nc = c.center.get_values();
                    nc[i] = c.center[i] - U[i];
                    new_coords.push_back(nc);
                }

                if (under[i] && over[1-i]) {
                    std::array<double,2> nc = c.center.get_values();
                    nc[i] = c.center[i] + U[i];
                    nc[1-i] = c.center[1-i] - U[1-i];
                    new_coords.push_back(nc);
                }

                if (under[i] && under[1-i]) {
                    std::array<double,2> nc = c.center.get_values();
                    nc[i] = c.center[i] + U[i];
                    nc[1-i] = c.center[1-i] + U[1-i];
                    new_coords.push_back(nc);
                }
                
                if (over[i] && over[1-i]) {
                    std::array<double,2> nc = c.center.get_values();
                    nc[i] = c.center[i] - U[i];
                    nc[1-i] = c.center[1-i] - U[1-i];
                    new_coords.push_back(nc);
                }
            }
            for(auto nc : new_coords) {
                ms.push_back(Circle(Coord(nc), c.radius, c.identifier));
            }
        }
       
        /* Ensure that the list contains only unique circles */
        std::sort(ms.begin(), ms.end(), cmp_circ);
        auto it = std::unique(ms.begin(), ms.end(), eq_circ);
        for(auto m = ms.begin(); m != it; ++m) {
            //std::cout << m->center[0] << ", " << m->center[1] << " " << m->radius << std::endl;
            circles.push_back(*m);
        }
    }

    struct IntersectionComparator {
        IntersectionComparator(Box b) : target_box(b) {}

        bool operator() (Circle &a, Circle &b) { 
            return target_box.area_of_intersection(a.bounding_box) > target_box.area_of_intersection(b.bounding_box);
        }
        Box target_box;
    };

    struct CoverCell {
        CoverCell(Box b, circle_container cs) : bb(b), circles(cs), completely_covered(false)  {
            for(auto &c : circles) {
                if (c.covers(b)) {
                    completely_covered = true;
                }
            }

            /* Sort by area covered by bucket so that circles that cover most are checked first */
            std::sort (circles.begin(), circles.end(), IntersectionComparator(b));
        }

        /* Check whether this bucket contains a circle containing point p */
        bool is_covered(Coord p) const {
            if (completely_covered) return true;
            for(auto &c : circles) {
                if (c.contains(p)) { 
                    return true;
                }
            }
            return false;
        }

        /* Return list to the circle that covers the point */
        std::list<Circle> which_covers(Coord p) const {
            auto cs = std::list<Circle>();
            for(const Circle &c : circles) {
                if (c.contains(p)) { 
                    cs.push_front(c);
                }
            }
            return cs;
        }

        Box bb; /* bounding box of this cell */
        circle_container circles; /* List of circles touching this bucket */
        bool completely_covered; /* Boolean flag to denote whether this bucket is completely covered */
    };

    circle_container circles;
    int granularity; /* divide the area given by U into granularity x granularity grid */
    Coord U; /* domain */
    std::vector<CoverCell> bins;
    double height; /* height of a single cell */
};

}
#endif // __FRAGMENT_H_
