#define CATCH_CONFIG_MAIN  
#include "catch.hpp"

#include "../pp.h"
#include "../fragments.h"

/**
 * Some basic tests for the data structures 
 **/


rng_t rng_instance;
std::vector<double> random_values(double max, int n) {
    std::vector<double> vs;
    for(int i = 0; i<n; i++) {
        vs.push_back(uniform_distribution(rng_instance) * max);
    }
    return vs;
}

using namespace pp;


TEST_CASE( "box", "[box]" ) {
    SECTION("Test box") {
        Box b(0.1, 1.0, 0.1, 1.0);
        Box b2(0.5, 0.9, 0.5, 0.9);
        Box b3(0.0, 2.0, 0.0, 2.0);
        Box b4(2.0, 3.0, 2.0, 3.0);

        REQUIRE( b.intersects(b2) );
        REQUIRE( b2.intersects(b) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );

        REQUIRE( b.intersects(b3) );
        REQUIRE( b3.intersects(b) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );

        REQUIRE( b2.intersects(b3) );
        REQUIRE( b3.intersects(b2) );
        REQUIRE( !b4.intersects(b) );
        REQUIRE( !b.intersects(b4) );

    }
}
    

TEST_CASE( "circle", "[circle]" ) {

    SECTION("Test circle") {
        Circle c(Coord(1.0,1.0),1);
        Coord p(1.0,1.0);
        REQUIRE( c.contains(p) );
        Coord q(0.0,1.5);
        REQUIRE( !c.contains(q) );

        Box b(0.0, 2.0, 0.0, 2.0);
        REQUIRE( c.intersects(b) ); // b contains c

        Box b2(0.0, 1.1, 0.0, 1.1); 
        REQUIRE( c.intersects(b2) ); // b2 intersects with c
    }
}

TEST_CASE( "cover query", "[CoverLookup]" ) {

    SECTION("Construction") {
        Coord U(10.0, 10.0);
        std::vector<Circle> circles;
        circles.push_back(Circle(1.0, 1.0, 1.0));
        circles.push_back(Circle(5.0, 5.0, 0.5));
        CoverLookup cq(circles, 10, U);

        std::cout << "CQ Constructed" << std::endl;

        SECTION("Test queries") {
            REQUIRE( cq.is_covered(Coord(1.0,1.0)) );
            REQUIRE( !cq.is_covered(Coord(1.0, 9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99, 1.0)) );
            REQUIRE( !cq.is_covered(Coord(9.99, 9.99)) );

            REQUIRE( !cq.is_covered(Coord(0.2,0.2)) );
            REQUIRE( cq.is_covered(Coord(1.3,0.8)) );
            REQUIRE( !cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(4.9,5.1)) );
            REQUIRE( !cq.is_covered(Coord(3.0,3.0)) );
            REQUIRE( !cq.is_covered(Coord(9.9,9.9)) );
        }
        
    }

    SECTION("Test overflowing borders (torus)") {
        Coord U(10.0, 10.0);
        
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(0.0, 0.0, 1.0));
            CoverLookup cq(circles, 10, U);

            REQUIRE( cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(9.99,0.0)) );
            REQUIRE( cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( cq.is_covered(Coord(9.99,9.99)) );

            REQUIRE( !cq.has_within(Coord(5.0, 5.0), 4.0) );
            REQUIRE( cq.has_within(Coord(1.0, 1.0), 1.0) );
            REQUIRE( cq.has_within(Coord(9.0, 9.0), 1.0) );
            REQUIRE( !cq.has_within(Coord(1.0, 9.0), 1.0) );
        }

        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(9.99, 9.99, 1.0));
            CoverLookup cq(circles, 10, U);

            REQUIRE( cq.is_covered(Coord(0.0,0.0)) );
            REQUIRE( cq.is_covered(Coord(9.99,0.0)) );
            REQUIRE( cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( cq.is_covered(Coord(9.99,9.99)) );
        }
       
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(0.0, 1.0, 1.0));
            CoverLookup cq(circles, 10, U);
            REQUIRE( cq.is_covered(Coord(0.0,1.0)) );
            REQUIRE( cq.is_covered(Coord(9.9,1.0)) );
            REQUIRE( !cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99,9.99)) );
        }

        
        SECTION("Test queries") {
            std::vector<Circle> circles;
            circles.push_back(Circle(9.9, 1.0, 1.0));
            CoverLookup cq(circles, 10, U);
            REQUIRE( cq.is_covered(Coord(0.0,1.0)) );
            REQUIRE( cq.is_covered(Coord(9.9,1.0)) );
            REQUIRE( !cq.is_covered(Coord(0.0,9.99)) );
            REQUIRE( !cq.is_covered(Coord(9.99,9.99)) );
        }
         
    }

    SECTION("Random input") {
        double U = 1000.0;
        int N = 1000;
        double tests = 5000;

        auto xs = random_values(U, N);
        auto ys = random_values(U, N);
        auto rs = random_values(U/4.0, N);
        std::vector<Circle> circles;
        for(int i = 0; i<N; i++) {
            circles.push_back(Circle(xs[i], ys[i], rs[i]));
        }

        CoverLookup cq(circles, 7, U);

        std::cout << "Running " << tests << " test cases" << std::endl;
        for(auto t = 0; t<tests; t++) {
            auto x = uniform_distribution(rng_instance) * U;
            auto y = uniform_distribution(rng_instance) * U;

            auto p = Coord(x,y);

            /* Test whether p is covered by some circle */
            bool covered = false;
            for(auto c : circles) {
                if (c.center.torus_squared_distance(p, U) <= c.radius*c.radius) {
                    covered = true;
                }
            }
            REQUIRE( covered == cq.is_covered(p) );

            /* Test that whether p intersects with some circle*/
            auto r = uniform_distribution(rng_instance) * U/2;
            bool within = false;
            for(auto c : circles) {
                double dist = (c.radius+r)*(c.radius+r);
                if (c.center.torus_squared_distance(p, U) <= dist) {
                    within = true;
                }
            }

            REQUIRE( within == cq.has_within(p, r) );
        }
    }

}
