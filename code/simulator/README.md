Compile the non-debug (i.e. fast) version with

    make

and the debug build can be compiled with

    make debug

The debug build will be much slower as it does various (slow) sanity checks during the simulation.

