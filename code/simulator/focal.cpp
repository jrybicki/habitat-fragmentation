#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <list>

#include "json.hpp"
using json = nlohmann::json;

#include "cxxopts.hpp"

#include "ppsim/pp.h"
#include "ppsim/fragments.h"

rng_t rng;
unsigned long SAMPLES = 10000;

cxxopts::Options parse_args(int argc, char *argv[]) {
    try {
        cxxopts::Options options("ppsim", "Point process simulator");
        options.positional_help("[optional args]");
        options.add_options()
          ("h,help", "Print help")
          ("i,input", "Input file", cxxopts::value<std::string>())
          ("s,seed", "Seed", cxxopts::value<unsigned long>())
          ("a,accuracy", "Sampling accuracy", cxxopts::value<unsigned long>())
          ;

        options.parse(argc, argv);

        if (options.count("help")) {
          std::cout << options.help({""}) << std::endl;
          exit(0);
        }
       
        return options;
    } catch (const cxxopts::OptionException& e) {
        std::cerr << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
}

pp::circle_container read_circles(json &input) {
    pp::circle_container cs;
    for(auto f : input["scaled.fragments"]) {
        pp::Circle c(pp::Coord(f["x"], f["y"]), f["r"]);
        cs.push_back(c);
    }
    return cs;
}

double approximate_overlap_area(pp::Box &b, pp::Circle &c) {
    auto k = pp::Tophat(1, c.radius);
    int hits = 0;

    for(int i = 0; i<SAMPLES; i++) {
        auto q = k.sample_around(rng, c.center);
        if (b.contains(q)) {
            hits += 1;
        }
    }

    double ratio = double(hits)/SAMPLES;
    return c.area*ratio;
}

double approximate_cover(pp::Box b, pp::circle_container &circles) {
    pp::circle_container contained; // circles completely contained
    pp::circle_container overlapping; // circles that just overlap

    for(auto &c : circles) {
        if (c.intersects(b)) {
            if (b.contains(c.bounding_box)) {
                contained.push_back(c);
            } else {
                overlapping.push_back(c);
            }
        }
    }

    double area_of_contained = 0.0;
    for(auto &c : contained) {
        area_of_contained += c.area;
    }

    double area_of_overlapping = 0.0;
    for(auto &c : overlapping) {
        area_of_overlapping += approximate_overlap_area(b,c);
    }

    //std::cout << "#Contained " << contained.size() << " #overlapping" << overlapping.size() << std::endl;

    return area_of_contained + area_of_overlapping;
}

std::vector<pp::Box> map_to_torus(std::vector<pp::Box> nbs, double U) {
    std::vector<pp::Box> bs;
    // map coords back to torus
    for (auto &b : nbs) {
        double nx0 = b.x0,
               nx1 = b.x1,
               ny0 = b.y0,
               ny1 = b.y1;

        if (b.x0 < 0) {
            assert(b.x1 == 0);
            nx0 = pp::wrap_coord(b.x0, U);
            nx1 = U;
        }
        
        if (b.x1 > U) {
            assert(b.x0 == U);
            nx0 = 0;
            nx1 = pp::wrap_coord(b.x1, U);
        }

        if (b.y0 < 0) {
            assert(b.y1 == 0);
            ny0 = pp::wrap_coord(b.y0, U);
            ny1 = U;
        }

        if (b.y1 > U) {
            assert(b.y0 == U);
            ny0 = 0;
            ny1 = pp::wrap_coord(b.y1, U);
        }
        bs.push_back(pp::Box(nx0,nx1,ny0,ny1));
    }
    return bs;
}

std::vector<pp::Box> split_box(pp::Box b, double U) {
    auto domain = pp::Box(0, U, 0, U);
    auto nbs = std::vector<pp::Box>();
    if (domain.contains(b)) {
        nbs.push_back(b);
    } else {
        // ABC
        // DEF
        // GHI

        // A
        if (b.y0 < 0 && b.x0 < 0) {
            nbs.push_back(
               pp::Box(b.x0, 0, b.y0, 0) 
            );
        }

        // B
        if (b.y0 < 0) {
            nbs.push_back(
               pp::Box(std::max(0.0, b.x0),std::min(U, b.x1), b.y0,  0) 
            );
        }

        // C
        if (b.y0 < 0 && b.x1 > U) {
            nbs.push_back(
               pp::Box(U, b.x1, b.y0, 0)
            );
        }

        // D
        if (b.x0 < 0) {
            nbs.push_back(
               pp::Box(b.x0, 0, std::max(0.0, b.y0), std::min(U, b.y1))
            );
        }

        // E 
        nbs.push_back(
            pp::Box(std::max(0.0, b.x0), 
                    std::min(U, b.x1), 
                    std::max(0.0, b.y0), std::min(U, b.y1))
        );

        // F
        if (b.x1 > U) {
            nbs.push_back(
               pp::Box(U, b.x1, std::max(0.0, b.y0), std::min(U, b.y1))
            );
        }

        // G
        if (b.x0 <0 && b.y1 > U) {
            nbs.push_back(
               pp::Box(b.x0,0,  U, b.y1)
            );
        }
        
        // H
        if (b.y1 > U) {
            nbs.push_back(
               pp::Box(std::max(0.0, b.x0),std::min(U, b.x1),  U, b.y1)
            );
        }

        // I
        if (b.x1 > U && b.y1 > U) {
            nbs.push_back(
               pp::Box(U, b.x1, U, b.y1)
            );
        }
    }

    return map_to_torus(nbs, U);
}

double cover_of_box(pp::circle_container cs, pp::Box q, double U) {
    auto boxes = split_box(q, U);
    double A = 0;
    for (auto b : boxes) {
        A += approximate_cover(b, cs);
    }
    return A;
}

json collect_data_from_sample_sites(json input) {
    auto input_cs = read_circles(input);
    double U = input["U"];
    auto cl = pp::CoverLookup(input_cs, 1, pp::Coord(U,U));
    auto cs = cl.get_circles(); 

    json output;

    for (auto site : input["sample.sites"]) {
        double cx = site["x"];
        double cy = site["y"];

        std::vector<json> results;
        for(int i = 1; i<=input["U"]; i++) {
            double d = double(i)/2.0;
            auto qbox = pp::Box(cx-d,cx+d,cy-d,cy+d);
            json r;
            r["size"] = i;
            r["cover"] = cover_of_box(cs, qbox, U) / double(i*i);
            results.push_back(r);
        }

        json data;
        data["x"] = cx;
        data["y"] = cy;
        data["results"] = results;
        output["data"].push_back(data);
    }
    return output;
}

int main(int argc, char **argv) {
    auto options = parse_args(argc, argv);

    if (options.count("seed"))
        rng.seed(options["seed"].as<unsigned long>());

    if (options.count("accuracy"))
        SAMPLES = options["accuracy"].as<unsigned long>();

    /* Read input data */
    
    //auto data_fname = options["input"].as<std::string>();
    //std::ifstream i(data_fname);
    json input; // input from analysis
    std::cin >> input;

    auto output = collect_data_from_sample_sites(input);
    std::cout << output;

#if 0
    auto input_cs = read_circles(input);

    for(auto &c : input_cs) {
        std::cout << c.center[0] << ", " << c.center[1] << ", " << c.radius << std::endl;
    }

    double U = input["U"];

    std::cout << "U = " << U << std::endl;

    auto cl = pp::CoverLookup(input_cs, 1, pp::Coord(U,U));

    auto cs = cl.get_circles(); 

    std::cout << "Mirrored" << std::endl;
    for(auto &c : cs) {
        std::cout << c.center[0] << ", " << c.center[1] << ", " << c.radius << std::endl;
    }

    for (auto offset_x : {-10, -20, -30, 1,2,3,4,5,6,7,8,9,10} ) {
    for (auto offset_y : {-10, -20, -30, 1,2,3,4,5,6,7,8,9,10} ) {
    auto q = pp::Box(U/2.0+offset_x, 3*U/2.0+offset_x, U/2.0+offset_y, 3*U/2.0+offset_y);
    auto boxes = split_box(q, U);
    std::cout << "Split into " << boxes.size() << " boxes" << std::endl;
    double A = 0;
    double bA = 0;
    for (auto b : boxes) {
        std::cout << b.x0 << ", " << b.y0 << "\t" << b.x1 << ", " << b.y1 << std::endl;
        double a = approximate_cover(b, cs);
        std::cout << "a = " << a << std::endl;
        A += a;
        bA += b.area();
    }

    std::cout << "A = " << A << std::endl;

    std::cout << "Query area = " << q.area() << " sum of query areas = " << bA << std::endl;
    }
    }
#endif
    return 0;
}
