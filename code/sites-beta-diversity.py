import sloss
import sys
import json
import util
import math
import numpy
import glob

def general_jaccard(x,y):
    a = sum([min(xi,yi) for (xi,yi) in zip(x,y)])
    b = sum([max(xi,yi) for (xi,yi) in zip(x,y)])
    return a/b

def euclidean(x,y):
    return sum((x-y)**2)**0.5

def sorensen(x,y):
    intersect = sum((x>0)*(y>0))
    pool = sum((x>0)+(y>0))
    return 2.0*intersect/float(pool)

def average_diversity(ss, metric):
    ls = list(ss)
    ds = []
    for i in xrange(len(ls)):
        x = sloss.get_proportions(ls[i])
        for j in xrange(i):
            y = sloss.get_proportions(ls[j])
            ds.append(metric(x,y))
    return numpy.average(ds)


diversity_measures = {
    'sorensen' : sorensen,
    'jaccard' : general_jaccard,
    'euclidean' : euclidean
}


rootdir = sys.argv[1]
data = sloss.read_data(rootdir)
order = 0 # order of the hill number

measures = sorted(diversity_measures.keys())
msr_str = ",".join(measures)

print "fragments,landscape.cover,replicate,sample.radius,total.sites,nonempty.sites,{}".format(msr_str)
for (fragments, cover), replicate_paths in data.items():
    for basedir in replicate_paths:

        community = json.load(util.open_file("{}/community.json".format(basedir)))
        entity_map = sloss.get_entity_map(community)
        for indata_fname in glob.glob("{}/output*json.gz".format(basedir)):
            indata = json.load(util.open_file(indata_fname))
            sites = indata["data"][-1]["sample.counts"]
            satiated_at_sites = [sloss.get_type_count("satiated", counts, entity_map) for counts in sites]

            if not "sample.sites" in indata:
                continue

            # Only consider sites with species
            total_sites = len(satiated_at_sites)
            filtered = [s for s in satiated_at_sites if sum(s) > 0]
            filtered_sites = len(filtered)
            if filtered_sites < 2:
                continue
            
            mdivs = [average_diversity(filtered, diversity_measures[m]) for m in measures]
            divstr = ",".join(map(str, mdivs))
            print "{},{},{},{},{},{},{}".format(fragments, cover, basedir.split("/")[-1],indata["sample.window.radius"],total_sites,filtered_sites,divstr)

