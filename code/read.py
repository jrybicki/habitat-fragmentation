import collections
from util import *
from shared import *

def read_pp_data(fp):
    Entry = collections.namedtuple('Entry', ['time', 'events', 'xs', 'ys', 'U'])
    U = None
    for line in fp:
        if line[0] == 'U':
            U = float(line.split(" ")[1])
            continue

        tokens = line.strip().split(" ")
        time = tokens[0]
        events = tokens[1]
        remaining = tokens[2:]

        pxs = collections.defaultdict(list)
        pys = collections.defaultdict(list)
        for (t, x, y) in grouper(3, remaining):
            tt = int(t)
            pxs[tt].append(float(x))
            pys[tt].append(float(y))

        yield Entry(time, events, pxs, pys, U)

def by_type(data):
    DataPoint = collections.namedtuple('DataPoint', ['time', 'xs', 'ys'])
    processed = collections.defaultdict(list)
    for entry in data:
        time, events, pxs, pys = entry
        for t in pxs.keys():
            xs = pxs[t]
            ys = pys[t]
            processed[t].append( DataPoint(time, xs, ys) )
    return processed
