import sys
import json
import random
import combinations
import fragmenter
import copy
import util

SIM_PATH = "./simulator/fragment-sim"
ANALYSE_PATH = "./simulator/analyse"

R = 1.0 # sample window size

parameters = {
    "variance" : [1.0],
    "cover" : [0.001, 0.005, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32],
    "fragments" : [1, 4, 16, 64, 256, 1024],
}

community = json.load(open(sys.argv[1]))
replicates = int(sys.argv[2])
outdir = sys.argv[3]
initial_seed = sys.argv[4]
initial_state_fname = sys.argv[5]
U = float(sys.argv[6])
T = float(sys.argv[7])
dT = float(sys.argv[8])

if len(sys.argv) > 9:
    parameters = json.load(open(sys.argv[9]))

random.seed(initial_seed)

def prepare(pars, seed):
    basedir = "{}/{}-{}/{}".format(outdir, pars["fragments"], pars["cover"], seed)
    util.new_dir(basedir)
    input_community_fname = "{}/community.json".format(basedir)
    output_snapshot_fname = "{}/out.snapshot".format(basedir)
    analysis_output_fname = "{}/output.json".format(basedir)
    density_output_fname = "{}/density.txt".format(basedir)
    
    # Generate input community file
    fragments = fragmenter.fragment(pars["fragments"], pars["cover"], pars["variance"])
    input_json = copy.deepcopy(community)
    input_json["fragments"] = [{"x" : f[0][0], "y" : f[0][1], "r" : f[1]} for f in fragments]
    json.dump(input_json, open(input_community_fname, 'w'))

    # Output batch
    simcmd = "{sim} -U {u} -t {t} --dt {dt} -o {snap} -d {dens} -m {input} -s {seed} -i {init}".format(sim=SIM_PATH, u=U, dt=dT, t=T, snap=output_snapshot_fname, input=input_community_fname, seed=seed, dens=density_output_fname, init=initial_state_fname)
    analysecmd = "{analyse} -s {t} -r {radius} -c {input} -i {snap} > {out} && gzip -f {out}".format(analyse=ANALYSE_PATH, t=T-1, radius=R, snap=output_snapshot_fname, input=input_community_fname, out=analysis_output_fname)
    print "{} && {}".format(simcmd, analysecmd)

for p in combinations.parameter_combinations(parameters):
    seeds = [random.randrange(1, 2**32-1) for i in xrange(replicates)]
    for s in seeds:
        prepare(p, s)
