# Supplementary material for "Habitat fragmentation and species diversity in competitive species communities"

    Habitat fragmentation and species diversity in competitive species communities
    Joel Rybicki, Nerea Abrego, and Otso Ovaskainen
    Date: June 2018

The repository is organised as follows:

* [`code/`](code/README.md) contains the source code and data files
* supplementary animations are given below

## Supplementary animations

The colours are as follows:

* black dots: resource particles
* coloured small dots: resource-deprived individuals
* coloured large dots: resource-satiated individuals

Different colours indicate different species.

### Short dispersal distance (𝛿=1)

#### Intact landscape (N=1, C=1.0)

![Intact landscape](./animations/1-animation-intact.gif "Intact landscape")

#### N=4 C=0.32

![N=4 C=0.32](./animations/1-animation-4-0.32.gif "N=4 C=0.32")

#### N=4 C=0.08

![N=4 C=0.08](./animations/1-animation-4-0.08.gif "N=4 C=0.32")

#### N=256 C=0.32

![N=256 C=0.32](./animations/1-animation-256-0.32.gif "N=256 C=0.32")

#### N=256 C=0.08

![N=4 C=0.08](./animations/1-animation-256-0.08.gif "N=4 C=0.08")

### Long dispersal distance (𝛿=10)

#### Intact landscape (N=1, C=1.0)

![Intact landscape](./animations/2-animation-intact.gif "Intact landscape")

#### N=4 C=0.32

![N=4 C=0.32](./animations/2-animation-4-0.32.gif "N=4 C=0.32")

#### N=4 C=0.08

![N=4 C=0.08](./animations/2-animation-4-0.08.gif "N=4 C=0.32")

#### N=256 C=0.32

![N=256 C=0.32](./animations/2-animation-256-0.32.gif "N=256 C=0.32")

#### N=256 C=0.08

![N=4 C=0.08](./animations/2-animation-256-0.08.gif "N=4 C=0.08")

